$(function(){

    resize();

    // Animation hover vignette
    $('.block').hoverIntent(function(){
        $(this).children('.description').toggleClass('active', 400);
        $(this).find('hr').toggleClass('active', 500);
    });

    function resize() {

        var block = $('.block')
        var description = $('.block .description')

        block.height(block.width())
        description.height(block.height())
        description.width($('.block img').width())

        /*
        vignette_dimension = $('.block').height();
        vignette_description = $('.block .description');

        vignette_description.css('height', vignette_dimension);
        vignette_description.css('width', $('.block img').width());

        $('.vignettes .block').css('height', vignette_dimension);
        */

    }

    $(window).on('resize', function() {
        resize();
    });

    // Ouvrir fenêtre groupe
    $('.block').click(function(){
        $.ajaxSetup({cache:false});
        $.ajax({
            url: 'assets/groupes/'+this.id+'/data.json',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function(data) {
                $('#groupe').empty();
                $('#template').tmpl(data).appendTo('#groupe');
                $('#groupe').show(400);
                $('.video-link,.player').magnificPopup({items:[{src:data.video,type:'iframe'}]});
                $('html,body').animate({scrollTop: $('#groupe').offset().top-25}, 'slow');
                // Bouton close
                $('.close').click(function(){
                    $('#groupe').hide(400).empty();
                    $('html,body').animate({scrollTop: $('.sous-titre').offset().top-45}, 'slow');
                });
            },
            complete: function() {

            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        })
    });

    // Fonctions
    function toggleActive(element) {

    }

    // Scroll To Top
    (function(){
        var offset = 500,
        offset_opacity = 1000,
        scroll_top_duration = 500,
        $back_to_top = $('.back-to-top');

        $(window).scroll(function() {
            ($(this).scrollTop() > offset) ? $back_to_top.addClass('visible') : $back_to_top.removeClass('visible');
        });

        $back_to_top.on('click', function(e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop: 0,
        }, scroll_top_duration);
        });
    })();

});
