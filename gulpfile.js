gulp = require('gulp');
browserSync = require('browser-sync').create(),
$ = require('gulp-load-plugins')();

const SOURCES = {
    scripts: 'assets/js/',
    styles: 'assets/scss/**/*.scss',
    html: '**/*.html'
}

const DEST = {
    css: 'assets/css/'
}

gulp.task('sass', function(done){
    gulp.src(SOURCES.styles)
    .pipe($.sass({
        onError: console.error.bind(console, 'SASS Error: ')
    }))
    .pipe($.autoprefixer({
        browsers: ['last 2 versions']
    }))
    .pipe(gulp.dest(DEST.css))
    done()
});

gulp.task('browsersync', gulp.series('sass'), function(){
    var files = [
        SOURCES.scripts,
        SOURCES.styles,
        SOURCES.html
    ];

    browserSync.init(files, {
        server: {
            baseDir: "./"
        }
    });

    gulp.watch(SOURCE.styles, gulp.series('sass'));

});

gulp.task('watch', function(){
    gulp.watch(SOURCES.styles, gulp.series('sass'));
})

gulp.task('default', gulp.series('sass'));
